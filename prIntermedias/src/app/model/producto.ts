
export class Producto {
    sku:string;
    codigo_barras:string;
    nombre:string;
    descripcion:string;
    precio:number;
    constructor(sku="",codigo_barras="",nombre="",descripcion="",precio=0.0){
        this.sku=sku;
        this.codigo_barras=codigo_barras;
        this.nombre=nombre;
        this.descripcion=descripcion;
        this.precio=precio;
    }
}
